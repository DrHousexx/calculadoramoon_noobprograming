<?php

    class Calculadora
    {
        //atrib
        public $nro1;
        public $nro2;

        //method
        public function Sumar()
        {
            //return $nro1 + $nro2;
            return $this->nro1 + $this->nro2;
        }
        public function Restar()
        {
            return $this->nro1 - $this->nro2;
        }
        public function Multiplicar()
        {
            return $this->nro1 * $this->nro2;
        }
        public function Dividir()
        {
            if($this->nro2 == 0)
            {
                return "No se puede dividr entre 0";
            }
            else
            {
                return $this->nro1 / $this->nro2;
            }
        }
        private function Fact($nro)
        {
            if($nro == 0)
            {
                return 1;
            }
            else
            {
                return $nro * $this->Fact($nro - 1);
            }
        }
        public function Factorial()
        {
            return $this->Fact($this->nro1);
        }


        private function Pote($base, $exp)
        {
            if($exp == 0)
            {
                return 1;
            }
            else{ 
                return $base * $this->Pote($base , $exp - 1);
            } 
        }
        public function Potencia()
        {
            return $this->Pote($this->nro1,$this->nro2);
        }



        private function Seno($nro) {
            return (sin(deg2rad($nro)));
        }
        private function Tangente($nro) {
            return (tan(($nro * pi()) / 180));
        }

        private function Coseno($nro) {
            return (cos(($nro * pi()) / 180));
        }
        public function Senou()
        {
            return $this->Seno($this->nro1);
        }
        public function Tangenteu()
        {
            return $this->Tangente($this->nro1);
        }
        public function Cosenou()
        {
            return $this->Coseno($this->nro1);
        }
        public function Porcentaje()
        {
            return ($this->nro1 * $this->nro2) / 100;
        }

        public function Inverso()
        {
            return (1/$this->nro1);
        }
        public function Raiz()
        {
            return sqrt($this->nro1);
        }
        public function RaizEnesima()
        {
            return (pow($this->nro1,(1/$this->nro2)));
        }
    }

?>
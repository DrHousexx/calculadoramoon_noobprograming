<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <title>Calculadora Basica ButterFly</title>
</head>
<body>
<div class="container-fluid div2 fondo">
    <form action="#" method = "POST">
    <div class="row hola bg-dark.bg-gradient">
        <div class="col-12">
        <legend><h1 class = "hola">🌙 Calculadora MooN (PHP) ༼ つ ◕_◕ ༽つ</h1></legend>
        </div>
    </div>
    <div class="row hola">
        <div class="col-12">
        Nro1: <input type="number" name = "txtnro1" class = "hola" >Nro2: <input type="number" name = "txtnro2" class = "hola" >
        </div>
    </div>
    <div class="row">
        <div class="col-12">
        
        </div>
    </div>
    <div class="row hola">
        <div class="col-2 ">
        
        </div>
        <div class="col-2 d-grid gap-2">
            <input type="submit" name = "btnSumar" value = "+" class = "hola2 btn-lg ">
        </div>
        <div class="col-2 d-grid gap-2 ">
            <input type="submit" name = "btnRestar" value = "-"  class = "hola2">
        </div>
        <div class="col-2 d-grid gap-2">
            <input type="submit" name = "btnMultiplicar" value = "X"  class = "hola2">
        </div>
        <div class="col-2 d-grid gap-2">
            <input type="submit" name = "btnDividir" value = "÷"  class = "hola2">
        </div>
        <div class="col-2 ">
        
        </div>
    </div>
    <div class="row hola">
        <div class="col-2 d-grid gap-2">
        
        </div>
        <div class="col-2 d-grid gap-2">
            <input type="submit" name = "btnFactorial" value = "X!" class = "hola2">
        </div>
        <div class="col-2 d-grid gap-2">
            <input type="submit" name = "btnPotencia" value = "X^n"  class = "hola2">
        </div>
        <div class="col-2 d-grid gap-2">
            <input type="submit" name = "btnSeno" value = "sen()"  class = "hola2">
        </div>
        <div class="col-2 d-grid gap-2">
            <input type="submit" name = "btnTangente" value = "tan()"  class = "hola2">
        </div>
        <div class="col-2 d-grid gap-2">
        
        </div>
    </div>
    <div class="row hola">
        <div class="col-1 d-grid gap-2">
        
        </div>
        <div class="col-2 d-grid gap-2">
            <input type="submit" name = "btnCoseno" value = "cos()" class = "hola2">
        </div>
        <div class="col-2 d-grid gap-2">
            <input type="submit" name = "btnPorcentaje" value = "%"  class = "hola2">
        </div>
        <div class="col-2 d-grid gap-2">
            <input type="submit" name = "btnRaiz" value = "√x"  class = "hola2 ">
        </div>
        <div class="col-2 d-grid gap-2">
            <input type="submit" name = "btnRaizN" value = "n√x"  class = "hola2">
        </div>
        <div class="col-2 d-grid gap-2">
            <input type="submit" name = "btnInverso" value = "x^-1"  class = "hola2">
        </div>
        <div class="col-1 d-grid gap-2">
        
        </div>
    </div>
    <div class="row hola">
        <div class="col-12 d-flex justify-content-center">
            RESULTADO: <input type="text" name = "txtr" id ="txt"class = "hola">
        </div>
    </div>
    </form>
</div>
    
    <?php
    if($_POST)
    {
        include("calculadora.php");
        $calculo = new Calculadora;

        //read
        $n1 = $_POST["txtnro1"];
        $n2 = $_POST["txtnro2"];
        $calculo->nro1 = $n1;
        $calculo->nro2 = $n2;

        function EscribirResultado($resultado)
        {   
            if(gettype($resultado) == "string")
            {
                ?>
                <script>   
                document.getElementById("txt").value = "Error de Argumentos"                
                </script>
            <?php
            }
            else{
                ?>
                <script>   
                document.getElementById("txt").value = <?php echo $resultado; ?>                
                </script>
            <?php
            }
        }

        //process
        if(isset($_POST["btnSumar"]))
        {
            if ($n2 == null || $n1 == null)
            {
                $varss = "Faltan Argumentos";
                EscribirResultado($varss);
            }
            else {
                EscribirResultado($calculo->Sumar());
            }
           
        }
        if(isset($_POST["btnMultiplicar"]))
        {
            if ($n2 == null || $n1 == null)
            {
                $varss = "Faltan Argumentos";
                EscribirResultado($varss);
            }
            EscribirResultado($calculo->Multiplicar());
        }
        if(isset($_POST["btnRestar"]))
        {
            if ($n2 == null || $n1 == null)
            {
                $varss = "Faltan Argumentos";
                EscribirResultado($varss);
            }
            else {
                EscribirResultado($calculo->Restar());
            }
            
        }
        if(isset($_POST["btnDividir"]))
        {
            if ($n2 == null || $n1 == null)
            {
                $varss = "Faltan Argumentos";
                EscribirResultado($varss);
            }
            else {
                EscribirResultado($calculo->Dividir());
            }
        }
        if(isset($_POST["btnFactorial"]))
        {
            EscribirResultado($calculo->Factorial());
        }
        if(isset($_POST["btnPotencia"]))
        {
            if ($n2 == null || $n1 == null)
            {
                $varss = "Faltan Argumentos";
                EscribirResultado($varss);
            }
            else {
                EscribirResultado($calculo->Potencia());
            }
            
        }
        if(isset($_POST["btnSeno"]))
        {
            EscribirResultado($calculo->Senou());
        }
        if(isset($_POST["btnCoseno"]))
        {
            EscribirResultado($calculo->Cosenou());
        }
        if(isset($_POST["btnTangente"]))
        {
            EscribirResultado($calculo->Tangenteu());
        }
        if(isset($_POST["btnInverso"]))
        {
            EscribirResultado($calculo->Inverso());
        }
        if(isset($_POST["btnPorcentaje"]))
        {
            if ($n2 == null || $n1 == null)
            {
                $varss = "Faltan Argumentos";
                EscribirResultado($varss);
            }
            else {
                EscribirResultado($calculo->Porcentaje());
            }
            
        }
        if(isset($_POST["btnRaiz"]))
        {
            EscribirResultado($calculo->Raiz());
        }
        if(isset($_POST["btnRaizN"]))
        {
            if ($n2 == null || $n1 == null)
            {
                $varss = "Faltan Argumentos";
                EscribirResultado($varss);
            }
            else {
                EscribirResultado($calculo->RaizEnesima());
            }   
        }

        
    }
    ?>
</body>
</html>